import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: '',
      home: MyWidget(),
    );
  }
}

class MyWidget extends StatefulWidget {
  const MyWidget({Key? key}) : super(key: key);

  @override
  State<MyWidget> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyWidget> {
  int _selectedIndex = 1;
  static const List<Widget> _widgetOptions = <Widget>[
    Text(
      '',
    ),
    Text(
      'Location',
    ),
    Text(
      'Deal',
    ),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Align(
            alignment: Alignment.centerLeft,
            child: Text(
              'MFU, Chiang Rai',
              style: TextStyle(color: Colors.black),
            )),
        backgroundColor: Colors.green,
      ),
      body: Column(
        children: [
          _widgetOptions.elementAt(_selectedIndex),
          searchBox(),
          divider(),
          deal(),
          divider(),
          prodlist1()
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.green,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.location_on),
            label: 'Location',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.price_check),
            label: 'Deal',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.white,
        onTap: _onItemTapped,
      ),
    );
  }
}

Widget searchBox() {
  return Container(
    margin: const EdgeInsets.all(20),
    padding: const EdgeInsets.all(10),
    alignment: Alignment.centerLeft,
    decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(
            color: Colors.black26, // Set border color
            width: 3.0), // Set border width
        borderRadius: const BorderRadius.all(
            Radius.circular(10.0)), // Set rounded corner radius
        boxShadow: const [
          BoxShadow(blurRadius: 10, color: Colors.black26, offset: Offset(1, 3))
        ] // Make rounded corner of border
        ),
    child: const Text("Type here to search"),
  );
}

Widget divider() => Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Expanded(
          child: Divider(
            indent: 20.0,
            endIndent: 10.0,
            thickness: 1,
          ),
        ),
      ],
    );

Widget deal() =>
    Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
      Text(
        'Hot Deal Around You',
        style: TextStyle(color: Colors.green),
      ),
    ]);

Widget prodlist1() {
  return Container(
    padding: const EdgeInsets.all(5),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Image.asset('assets/images/img_localdeal1.png',
            width: 140, height: 140),
        Column(
          children: [
            Text('ACER Chromebook\n 314 (CB314-1H\n-C5DC) voor\n'),
            Text('8077THB', style: TextStyle(fontSize: 18.0)),
            Row(
              children: [
                IconButton(
                  icon: const Icon(Icons.favorite, color: Colors.green),
                  tooltip: '1k',
                  onPressed: () {},
                ),
                Text('1.5k', style: TextStyle(color: Colors.black)),
                IconButton(
                  icon: const Icon(Icons.message_outlined, color: Colors.black),
                  tooltip: '300',
                  onPressed: () {},
                ),
                Text('250', style: TextStyle(color: Colors.black)),
              ],
            )
          ],
        ),
      ],
    ),
  );
}
